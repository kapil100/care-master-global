<?php

return [
  "paginate_per_page" => env('PAGINATE_DEFAULT', 10)
];
