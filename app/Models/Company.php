<?php

namespace App\Models;

use App\Mail\CompanyRegisteredMail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Mail;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'logo',
        'website',
    ];

    public function employees(){
        return $this->hasMany(Employee::class, 'company', 'id');
    }

    public function sendMail(){
        if($this->email){
            Mail::to($this->email)->send(new CompanyRegisteredMail($this));
            return true;
        }
        return false;
    }
}
