<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddEmployeeRequest;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    private $employee;
    public function __construct(Employee $employee){
        $this->employee = $employee;
    }
    public function index(){
        $employees = Employee::paginate(config('data.paginate_per_page'));
        return view('employee.index')->with([
            'employees' => $employees
        ]);
    }

    public function create(Request $request){
        $companies = Company::all();
        return view('employee.create')->with([
            'companies' => $companies
        ]);
    }

    public function store(AddEmployeeRequest $request){
        $employeeCreate = $this->employee->create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'company' => $request->company,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);
        if($employeeCreate){
            return redirect()->to('/employees');
        }
        return redirect()->back();
    }

    public function edit(Request $request, Employee $employee){
        $companies = Company::all();
        return view('employee.edit')->with([
            'employee' => $employee,
            'companies' => $companies
        ]);
    }

    public function update(AddEmployeeRequest $request, Employee $employee){
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->company = $request->company;
        $employee->save();
        return redirect()->to('/employees');
    }

    public function destroy(Request $request, Employee $employee){
        $employee->delete();
        return redirect()->back();
    }
}
