<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\AddCompanyRequest;
use Illuminate\Support\Facades\Storage;
use Session;
class CompanyController extends Controller
{
    private $company;
    public function __construct(Company $company){
        $this->company = $company;
    }
    public function index(){
        $companies = Company::paginate(config('data.paginate_per_page'));
        return view('company.index')->with([
            'companies' => $companies
        ]);
    }

    public function create(Request $request){
        return view('company.create');
    }

    public function store(AddCompanyRequest $request){
        $logo = '';
        $files=$request->file('logo');
        if($files){
            $fileName = $files->getClientOriginalName();
            $fileExtension = '.'.$files->getClientOriginalExtension();
            $logo = md5($fileName.microtime()).$fileExtension;
            Storage::disk('local')->putFileAs('public/company', $files, $logo);
        }
        $companyCreate = $this->company->create([
           'name' => $request->name,
           'email' => $request->email,
           'logo' => $logo,
            'website' => $request->website
        ]);
        $companyCreate->sendMail();
        if($companyCreate){
            return redirect()->to('/companies');
        }
        return redirect()->back();
    }

    public function edit(Request $request, Company $company){
        return view('company.edit')->with([
            'company' => $company
        ]);
    }

    public function update(AddCompanyRequest $request, Company $company){
        $logo = $company->logo;
        $files=$request->file('logo');
        if($files){
            $fileName = $files->getClientOriginalName();
            $fileExtension = '.'.$files->getClientOriginalExtension();
            $newLogo = md5($fileName.microtime()).$fileExtension;
            Storage::delete('public/company/'.$logo);
            $logo= $newLogo;
            Storage::disk('local')->putFileAs('public/company', $files, $logo);
        }
        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->logo = $logo;
        $company->save();
        return redirect()->to('/companies');
    }

    public function show(Request $request, Company $company){
        return view('company.show')->with([
            'company' => $company
        ]);
    }

    public function destroy(Request $request, Company $company){
        $company->delete();
        return redirect()->back();
    }
}
