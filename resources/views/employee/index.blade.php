@extends('layout.admin')
@section('content')

    <div class="m-4">
        <div class="text-end">
            <a href="{{route('employee.create')}}" class="btn btn-primary">Add New</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Company</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td>{{$employee->first_name.' '.$employee->last_name}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->phone}}</td>
                    <td>{{$employee->myCompany->name}}</td>
                    <td>
                        <a href="{{route('employee.edit', $employee->id)}}">edit</a>
                        <button class="btn btn-link" onclick="event.preventDefault();
                            document.getElementById('employee-delete-'+{{$employee->id}}).submit();">
                            Delete
                        </button>
                        <form id="{{'employee-delete-'.$employee->id}}" action="{{route('employee.destroy', $employee->id)}}" method="post" class="d-none">
                            {{method_field('delete')}}
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$employees->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection
