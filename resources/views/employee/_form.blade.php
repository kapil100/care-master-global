<div class="mb-3">
    <label for="first_name" class="form-label">First Name</label>
    <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{(isset($employee->first_name)? $employee->first_name:old('first_name'))}}"/>
    @if($errors->has('first_name'))
        <span class="text-danger">{{ $errors->first('first_name') }}</span>
    @endif
</div>
<div class="mb-3">
    <label for="last_name" class="form-label">Last Name</label>
    <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{(isset($employee->last_name)? $employee->last_name:old('last_name'))}}"/>
    @if($errors->has('last_name'))
        <span class="text-danger">{{ $errors->first('last_name') }}</span>
    @endif
</div>
<div class="mb-3">
    <label for="email" class="form-label">Email</label>
    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{(isset($employee->email)? $employee->email:old('email'))}}"/>
    @if($errors->has('email'))
        <span class="text-danger">{{ $errors->first('email') }}</span>
    @endif
</div>
<div class="mb-3">
    <label for="phone" class="form-label">Phone</label>
    <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{(isset($employee->phone)? $employee->phone:old('phone'))}}"/>
    @if($errors->has('phone'))
        <span class="text-danger">{{ $errors->first('phone') }}</span>
    @endif
</div>
<div class="mb-3">
    <label class="col-md-12 col-form-label">Company</label>
    <select class="form-control  @error('company') is-invalid @enderror" name="company">
        <option value="">Select Company</option>
        @foreach($companies as $company)
        <option value="{{$company->id}}" {{(isset($employee->company) && $employee->company == $company->id) ? 'selected': ''}}>{{$company->name}}</option>
        @endforeach
    </select>
    @if($errors->has('company'))
        <span class="text-danger">{{ $errors->first('company') }}</span>
    @endif
</div>
