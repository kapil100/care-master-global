@extends('layout.admin')
@section('content')
    <div class="m-auto card p-2">
        <div class="card-body">
            <h5 class="card-title">Update Employee</h5>
            <form method="post" action="{{ route('employee.update', $employee->id) }}" enctype="multipart/form-data">
                {{method_field('put')}}
                @csrf
                @include('employee._form')
                <div class="d-flex justify-content-between">
                    <a href="{{route('employee.index')}}" class="btn btn-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
