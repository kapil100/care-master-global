<nav class="navbar navbar-dark bg-primary">
    <div class="container-fluid">
        @include('layout.sidenav')
        <div class="d-flex">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                <p class="mb-0 ml-2 text-white">Logout</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </div>
</nav>
