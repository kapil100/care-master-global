<button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasNavbarLabel">{{env('APP_NAME', '')}}</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
            <li class="nav-item bg-primary px-2 mt-2">
                <a class="nav-link active text-black" aria-current="page" href="{{route('dashboard')}}">Home</a>
            </li>
            <li class="nav-item dropdown bg-primary px-2 mt-2">
                <a class="nav-link dropdown-toggle text-black" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Company
                </a>
                <ul class="dropdown-menu bg-primary border-none" aria-labelledby="offcanvasNavbarDropdown">
                    <li><a class="dropdown-item text-black" href="{{route('company.index')}}">List</a></li>
                    <li><a class="dropdown-item text-black" href="{{route('company.create')}}">Create</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown bg-primary px-2 mt-2">
                <a class="nav-link dropdown-toggle text-black" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Employee
                </a>
                <ul class="dropdown-menu bg-primary border-none" aria-labelledby="offcanvasNavbarDropdown">
                    <li><a class="dropdown-item text-black" href="{{route('employee.index')}}">List</a></li>
                    <li><a class="dropdown-item text-black" href="{{route('employee.create')}}">Create</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
