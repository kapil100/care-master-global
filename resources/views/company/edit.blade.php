@extends('layout.admin')
@section('content')
    <div class="m-auto card p-2">
        <div class="card-body">
            <h5 class="card-title">Update Company</h5>
            <form method="post" action="{{ route('company.update', $company->id) }}" enctype="multipart/form-data">
                {{method_field('put')}}
                @csrf
                @include('company._form')
                <div class="d-flex justify-content-between">
                    <a href="{{route('company.index')}}" class="btn btn-secondary">Back</a>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
