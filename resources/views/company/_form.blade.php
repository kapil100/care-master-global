<div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{(isset($company->name)? $company->name:old('name'))}}"/>
    @if($errors->has('name'))
        <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
</div>
<div class="mb-3">
    <label for="email" class="form-label">Email</label>
    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{(isset($company->email)? $company->email:old('email'))}}"/>
    @if($errors->has('email'))
        <span class="text-danger">{{ $errors->first('email') }}</span>
    @endif
</div>
<div class="mb-3">
    <label for="website" class="form-label">Website</label>
    <input type="text" class="form-control @error('website') is-invalid @enderror" name="website" value="{{(isset($company->website)? $company->website:old('website'))}}"/>
    @if($errors->has('website'))
        <span class="text-danger">{{ $errors->first('website') }}</span>
    @endif
</div>
<div class="mb-3">
    <label for="logo" class="form-label">Logo</label>
    <input class="form-control form-control-lg @error('logo') is-invalid @enderror" name="logo" type="file" onchange="loadFile(event)" id="logo">
    @if($errors->has('logo'))
        <span class="text-danger">{{ $errors->first('logo') }}</span>
    @endif
    <span class="text-muted">Minimum dimension 100 x 100</span>
    <div class="image">
        @if(isset($company->logo))
            <img id="output" alt="" width="100" height="100" src="{{asset('storage/company/'.$company->logo)}}"/>
        @endif
    </div>
</div>


<script>
    const loadFile = function(event) {
        const img = new Image()
        img.src = window.URL.createObjectURL(event.target.files[0]);
        img.onload = function(){
            let output = document.getElementById('output');
            if(output){
                output.src = img.src;
            }else{
                $('<img src="" id="output" width="100" height="100"/>').appendTo('.image');
                output = document.getElementById('output');
                output.src = img.src;
            }

        }
    };
</script>
