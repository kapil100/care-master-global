@extends('layout.admin')
@section('content')

    <div class="m-4">
        <div class="text-end">
            <a href="{{route('company.create')}}" class="btn btn-primary">Add New</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Company Name</th>
                <th scope="col">Email</th>
                <th scope="col">Website</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
            <tr>
                <td>
                    <div>
                        @if($company->logo)
                            <img src="{{asset('storage/company/'.$company->logo)}}" width="100" height="100" class="rounded img-thumbnail" />
                        @endif
                            {{$company->name}}
                    </div>
                </td>
                <td>{{$company->email}}</td>
                <td>{{$company->website}}</td>
                <td>
                    <a href="{{route('company.show', $company->id)}}">show</a>
                    <a href="{{route('company.edit', $company->id)}}">edit</a>
                    <button class="btn btn-link" onclick="event.preventDefault();
                          document.getElementById('company-delete-'+{{$company->id}}).submit();">
                        Delete
                    </button>
                    <form id="{{'company-delete-'.$company->id}}" action="{{route('company.destroy', $company->id)}}" method="post" class="d-none">
                        {{method_field('delete')}}
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        {{$companies->links('vendor.pagination.bootstrap-4')}}
    </div>
@endsection
