@extends('layout.admin')
@section('content')
    <div class="card w-1/2 m-auto">
        <div class="card-body">
            <h5 class="card-title">{{$company->name}}</h5>
            <p class="text-16 bold underline">Company Detail</p>
            <div class="border p-2">
                <p>Email: {{$company->email}}</p>
                <p>Website: {{$company->website}}</p>
                <p>
                    <span class="bold">Employees:</span>
                    <br>
                    @foreach($company->employees as $employee)
                        <span>Name: {{$employee->first_name.' '.$employee->last_name}}</span>
                        <br>
                        <span>Email: {{$employee->email}}</span>
                        <br>
                        <span>Phone: {{$employee->phone}}</span>
                        <br>
                    @endforeach
                </p>
            </div>
            <a href="{{route('company.index')}}" class="btn btn-secondary mt-2">Back</a>
        </div>
    </div>
@endsection
