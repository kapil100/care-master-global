<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getAdmin(){
        return User::factory()->create([
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);
    }
}
