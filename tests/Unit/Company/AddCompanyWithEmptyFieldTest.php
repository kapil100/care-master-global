<?php

namespace Tests\Unit\Company;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class AddCompanyWithEmptyFieldTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testAddCompany()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $companyPostRoute = route('company.store');
        $companyAddRoute = route('company.create');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($companyAddRoute)->assertSee('create');
        $response = $this->post($companyPostRoute, $this->data());
        /**
         * Assert
         */
        $response->assertRedirect($companyAddRoute);
        $this->assertDatabaseMissing($companyModel->getTable(), $this->data());
        $response->assertSessionHasErrors(['name']);
    }

    private function data(): array {
        return [
            'name' => '',
            'logo' => '',
            'email' => '',
            'website' => ''
        ];
    }
}
