<?php


namespace Tests\Unit\Company;
use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteCompanyTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testDeleteCompany()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $company = $companyModel->factory()->create();
        $companyDeleteRoute = route('company.destroy', $company->id);
        /**
         * Act
         */
        $this->actingAs($this->getAdmin());
        $this->delete($companyDeleteRoute);
        /**
         * Assert
         */
        $this->assertSoftDeleted('companies', $company->fresh()->toArray());
    }
}
