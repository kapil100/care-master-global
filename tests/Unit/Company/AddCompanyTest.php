<?php

namespace Tests\Unit\Company;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class AddCompanyTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testAddCompany()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $data = $companyModel->factory()->make();
        $companyPostRoute = route('company.store');
        $companyAddRoute = route('company.create');
        $companyRoute = route('company.index');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($companyAddRoute)->assertSee('create');
        $response = $this->post($companyPostRoute, $data->toArray());
        /**
         * Assert
         */
        $this->assertDatabaseHas($companyModel->getTable(), $data->toArray());
        $response->assertRedirect($companyRoute);
    }
}
