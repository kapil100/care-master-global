<?php


namespace Tests\Unit\Company;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class CompanyEmailValidationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testCompanyEmailWithoutValid()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $data = $companyModel->factory()->make([
            'email' => 'check_email'
        ]);
        $companyPostRoute = route('company.store');
        $companyAddRoute = route('company.create');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($companyAddRoute)->assertSee('create');
        $response = $this->post($companyPostRoute, $data->toArray());
        /**
         * Assert
         */
        $response->assertRedirect($companyAddRoute);
        $this->assertDatabaseMissing($companyModel->getTable(), $data->toArray());
        $response->assertSessionHasErrors(['email']);
        $response->assertSessionDoesntHaveErrors(['name', 'logo', 'website']);
    }
    public function testCompanyEmailWithValid()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $data = $companyModel->factory()->make();
        $companyPostRoute = route('company.store');
        $companyAddRoute = route('company.create');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($companyAddRoute)->assertSee('create');
        $response = $this->post($companyPostRoute, $data->toArray());
        /**
         * Assert
         */
        $this->assertDatabaseHas($companyModel->getTable(), $data->toArray());
        $response->assertSessionDoesntHaveErrors(['name', 'logo', 'website', 'email']);
    }
    public function testCompanyEmailWithEmpty()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $data = $companyModel->factory()->make([
            'email' => null
        ]);
        $companyPostRoute = route('company.store');
        $companyAddRoute = route('company.create');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($companyAddRoute)->assertSee('create');
        $response = $this->post($companyPostRoute, $data->toArray());
        /**
         * Assert
         */
        $this->assertDatabaseHas($companyModel->getTable(), $data->toArray());
        $response->assertSessionDoesntHaveErrors(['name', 'logo', 'website', 'email']);
    }
}
