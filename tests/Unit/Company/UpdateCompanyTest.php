<?php


namespace Tests\Unit\Company;


use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateCompanyTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testUpdateCompany()
    {
        /**
         * Arrange
         */
        $companyModel = new Company;
        $company = $companyModel->factory()->create();
        $newData = $companyModel->factory()->make();
        $companyPutRoute = route('company.update', $company->id);
        $companyEditRoute = route('company.edit', $company->id);
        $companyRoute = route('company.index');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($companyEditRoute)->assertSee('Update');
        $response = $this->put($companyPutRoute, $newData->toArray());
        /**
         * Assert
         */
        $this->assertDatabaseHas($companyModel->getTable(), $newData->toArray());
        $response->assertRedirect($companyRoute);
    }
}
