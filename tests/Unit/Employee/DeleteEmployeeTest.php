<?php


namespace Tests\Unit\Employee;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteEmployeeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testDeleteEmployee()
    {
        /**
         * Arrange
         */
        $employeeModel = new Employee;
        $employee = $employeeModel->factory()->create();
        $employeeDeleteRoute = route('employee.destroy', $employee->id);
        /**
         * Act
         */
        $this->actingAs($this->getAdmin());
        $this->delete($employeeDeleteRoute);
        /**
         * Assert
         */
        $this->assertSoftDeleted('employees', $employee->fresh()->toArray());
    }
}
