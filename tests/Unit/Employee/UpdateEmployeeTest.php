<?php


namespace Tests\Unit\Company;

use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateEmployeeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testUpdateEmployee()
    {
        /**
         * Arrange
         */
        $employeeModel = new Employee;
        $employee = $employeeModel->factory()->create();
        $newData = $employeeModel->factory()->make();
        $employeePutRoute = route('employee.update', $employee->id);
        $employeeEditRoute = route('employee.edit', $employee->id);
        $employeeRoute = route('employee.index');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($employeeEditRoute)->assertSee('Update');
        $response = $this->put($employeePutRoute, $newData->toArray());
        /**
         * Assert
         */
        $this->assertDatabaseHas($employeeModel->getTable(), $newData->toArray());
        $response->assertRedirect($employeeRoute);
    }
}
