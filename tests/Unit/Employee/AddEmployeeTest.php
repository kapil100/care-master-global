<?php

namespace Tests\Unit\Employee;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class AddEmployeeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testAddEmployee()
    {
        /**
         * Arrange
         */
        $employeeModel = new Employee;
        $data = $employeeModel->factory()->make();
        $employeePostRoute = route('employee.store');
        $employeeAddRoute = route('employee.create');
        $employeeRoute = route('employee.index');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($employeeAddRoute)->assertSee('create');
        $response = $this->post($employeePostRoute, $data->toArray());
        /**
         * Assert
         */
        $this->assertDatabaseHas($employeeModel->getTable(), $data->toArray());
        $response->assertRedirect($employeeRoute);
    }
}
