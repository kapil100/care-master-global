<?php

namespace Tests\Unit\Company;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
class AddEmployeeWithEmptyFieldTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function testAddEmployeeWithEmptyField()
    {
        /**
         * Arrange
         */
        $employeeModel = new Employee;
        $employeePostRoute = route('employee.store');
        $employeeAddRoute = route('employee.create');
        /**
         * Act
         */
        $response = $this->actingAs($this->getAdmin());
        $response->get($employeeAddRoute)->assertSee('create');
        $response = $this->post($employeePostRoute, $this->data());
        /**
         * Assert
         */
        $response->assertRedirect($employeeAddRoute);
        $this->assertDatabaseMissing($employeeModel->getTable(), $this->data());
        $response->assertSessionHasErrors(['first_name', 'last_name', 'company']);
    }

    private function data(): array {
        return [
            'first_name' => '',
            'last_name' => '',
            'email' => '',
            'phone' => '',
            'company' => ''
        ];
    }
}
