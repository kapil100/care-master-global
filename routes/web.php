<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('guest')->group(function(){
    Route::get('/', 'Auth\LoginController@showLogin')->name('login.show');
    Route::post('/', 'Auth\LoginController@login')->name('login');
});
Route::middleware('auth')->group(function(){
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    //company
    Route::get('/companies', 'CompanyController@index')->name('company.index');
    Route::resource('company', 'CompanyController')->except('index');

    //employees
    Route::get('/employees', 'EmployeeController@index')->name('employee.index');
    Route::resource('employee', 'EmployeeController')->except('index', 'show');

});
