<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $companyName = $this->faker->company();
        return [
            'email' => $this->faker->unique()->safeEmail(),
            'name' => $companyName,
            'website' => 'http://www.'.$companyName.'.com.au'
        ];
    }
}
